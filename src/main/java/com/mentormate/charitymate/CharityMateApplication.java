package com.mentormate.charitymate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CharityMateApplication {

	public static void main(String[] args) {
		SpringApplication.run(CharityMateApplication.class, args);
	}

}

